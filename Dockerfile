FROM gitlab/gitlab-runner:latest
RUN apt update -y && apt install -y unzip sudo 
RUN wget https://releases.hashicorp.com/terraform/1.4.7/terraform_1.4.7_linux_amd64.zip \
    && unzip terraform_1.4.7_linux_amd64.zip \
    && mv terraform /usr/local/bin/ \
    && terraform --version

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && sudo ./aws/install \
    && rm -rf *.zip \
    && aws --version

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 \
    && chmod 700 get_helm.sh \
    && ./get_helm.sh \
    && rm -f get_helm.sh

